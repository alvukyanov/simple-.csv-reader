//
// Created by artyom on 16.01.2022.
//

#include <iostream> // console out
#include <fstream> // for files
#include <vector>
#include <unordered_map>
#include <utility> // for pair
#include <string>

enum class Operation {
    add,
    subtract,
    multiply,
    divide
};

struct ArgsHolder {
    std::pair<size_t, size_t> first_pos; // coordinates of 1st formula argument
    std::pair<size_t, size_t> second_pos; // coordinates of 2nd formula argument
    std::pair<Operation, size_t> operation; // coordinate of arithmetic sign between 1st & 2nd with sign itself
};

const std::unordered_map<char, Operation> operationsMap = { // hash-table with arithmetic signs
    {'+', Operation::add},
    {'-', Operation::subtract},
    {'*', Operation::multiply},
    {'/', Operation::divide}
};

std::pair<Operation, size_t> findOperationPosition (const std::string& operation) {  // search of delimiter position
    std::pair<Operation, size_t> position;
    for (size_t i = 0; i < operation.length(); ++i) {
        auto it = operationsMap.find(operation[i]);
        if (it != operationsMap.end()) {
            position.first = it->second;
            position.second = i;
            break;
        }
    }
    return position;
}

std::unordered_map<std::string, size_t> getCols(std::vector<std::vector<std::string>> &data) { // get the Columns for our data
    std::unordered_map<std::string, size_t> columns;
    for (size_t j = 1; j < data[0].size(); j++) {
        columns[data[0][j]] = j;
    }
    return columns;
}

std::unordered_map<std::string, size_t> getRows(std::vector<std::vector<std::string>> &data) { // get the rows for our data
    std::unordered_map<std::string, size_t> rowNumbers;
    for (size_t j = 1; j < data.size(); j++) {
        rowNumbers[data[j][0]] = j;
    }
    return rowNumbers;
}

std::pair<size_t, size_t> getArgument(  // parser for finding and defining the arguments
    std::string &str,
    std::unordered_map<std::string, size_t> &columns,
    std::unordered_map<std::string, size_t> &row_numbers) {
    std::string colsLine, rowsLine, argument;
    size_t rowNumberIndex = 0, colNumberIndex = 0;
    for (size_t i = 1; i < str.length(); ++i) {
        colsLine = str.substr(0, i);
        rowsLine = str.substr(i, str.length() - i);
        if ((row_numbers.find(rowsLine) != row_numbers.end())
            && (columns.find(colsLine) != columns.end())) {
            colNumberIndex = row_numbers[rowsLine];
            rowNumberIndex = columns[colsLine];
            return std::make_pair(colNumberIndex, rowNumberIndex); // get the positions of arg in search
        }
    }
    return std::make_pair<size_t, size_t>(0, 0);
}


ArgsHolder getArgs( // sub-function, that defines all args for completing the formula
    std::string &argument,
    std::unordered_map<std::string, size_t> &columns,
    std::unordered_map<std::string, size_t> &row_numbers) {
    ArgsHolder args;
    args.operation = findOperationPosition(argument);
    std::string buffer = argument.substr(1, args.operation.second - 1);
    args.first_pos = getArgument(buffer, columns, row_numbers);
    buffer = argument.substr(args.operation.second + 1, argument.length() - args.operation.second + 1);
    args.second_pos = getArgument(buffer, columns, row_numbers);
    return args;
}

void compute( // recursive func, that brings the result of formula, which starts with '='
    std::string data_str,
    std::vector<std::vector<std::string>> &data,
    std::unordered_map<std::string, size_t> &columns,
    std::unordered_map<std::string, size_t> &row_numbers,
    std::pair<size_t, size_t> &pos) {
    const std::string err = "!ERRR";
    auto arguments = getArgs(data_str, columns, row_numbers);
    auto first_value = data[arguments.first_pos.first][arguments.first_pos.second]; // get the formula element #1
    auto second_value = data[arguments.second_pos.first][arguments.second_pos.second]; // get the formula element #2
    if ((first_value == err) || (second_value == err)){  // correct data check
        data[pos.first][pos.second].clear();
        data[pos.first][pos.second] = err;
    }
    if (first_value[0] == '=') {
        compute(first_value, data, columns, row_numbers, arguments.first_pos);
    }
    if (second_value[0] == '=') {
        compute(second_value, data, columns, row_numbers, arguments.second_pos);
    }
    long double ARG1 = std::stod(data[arguments.first_pos.first][arguments.first_pos.second]),
                ARG2 = std::stod(data[arguments.second_pos.first][arguments.second_pos.second]), result;
    switch (arguments.operation.first) {
        case Operation::add: {
            result = ARG1 + ARG2;
            break;
        }
        case Operation::subtract: {
            result = ARG1 - ARG2;
            break;
        }
        case Operation::multiply: {
            result = ARG1 * ARG2;
            break;
        }
        case Operation::divide: {
            if (ARG2 == 0){
                data[pos.first][pos.second].clear();
                data[pos.first][pos.second] = err;
            } else {
                result = ARG1 / ARG2;
                break;
            }
        }
    }
    if (data[pos.first][pos.second] != err) {
        data[pos.first][pos.second] = std::to_string(result);
    }
}

bool input(std::string &filePath, std::vector<std::vector<std::string>> &data) { // input, if file is opened successfully
    std::ifstream file(filePath);
    if (!file) {
        std::cout << "File not found\n";
        return false;
    }
    std::string line;
    while (getline(file, line)) {
        data.emplace_back(1, "");
        for (char k : line) {
            if (k == ',') {
                data.back().emplace_back();
            } else {
                data.back().back().push_back(k);
            }
        }
    }
    file.close();
    return true;
}

bool evaluate(std::vector<std::vector<std::string>> &data) { // main computing function, that modifies all data
    auto columns = getCols(data);
    auto row_numbers = getRows(data);
    for (size_t i = 1; i < data.size(); i++) {
        if (data[i].size() != data[0].size()) {
            std::cout << "Wrong shape\n";
            return false;
        }
        for (size_t j = 1; j < data[i].size(); j++) {
            if (data[i][j][0] == '=') {
                auto pos = std::make_pair(i, j);
                compute(data[i][j], data, columns, row_numbers, pos);
            }
        }
    }
    return true;
}

void output(std::vector<std::vector<std::string>> &data) { // default console output
    for (auto &vector: data) {
        for (auto &value: vector) {
            std::cout << value << " ";
        }
        std::cout << " \n";
    }
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "Wrong number of console args\n";
        return 0;
    }
    std::string filePath = argv[1];
    std::vector<std::vector<std::string>> data;  // vector with data from file;
    if (!input(filePath, data)) { // fileinput of data
        return 0;
    }
    if (!evaluate(data)) { // computing
        return 0;
    }
    output(data);
    return 0;
}